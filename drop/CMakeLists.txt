cmake_minimum_required (VERSION 2.6)
project (ncencoder)
include_directories(../filter/include)
add_executable(
drop 
src/drop.c
src/cmd_args.c
)
set(CMAKE_C_FLAGS "-std=gnu99 -lnetfilter_queue -pthread -lrt")
target_link_libraries(drop)