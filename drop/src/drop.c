/*
 ============================================================================
 Name        : ncencoder.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "cmd_args.h"
#include "filter.h"

double drop = 0;

int rand2(int lim) {
	static long a = 1; // could be made the seed value
	a = (a * 32719 + 3) % 32749;
	return ((a % lim) + 1);
}

static int drop_handler(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
		struct nfq_data *nfa, void *data) {
	u_int32_t id;
	struct nfqnl_msg_packet_hdr *ph;
	ph = nfq_get_msg_packet_hdr(nfa);
	if (ph) {
		id = ntohl(ph->packet_id);
	} else {
		perror("msg_packet_hdr is null");
	}
	if (rand2(100000000) < drop * 1000000) {
		return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
	} else {
		return nfq_set_verdict(qh, id, NF_QUEUE || (1 << 16), 0, NULL);
	}
}

int main(int argc, char **argv) {
	CommandLineArgs args;
	if (argc == 1) {
		printf("Usage: \n\n");
		cmd_show_usage();
	} else {
		cmd_parse_args(&args, argc, argv, NULL, NULL);
		printf("Drop started with the following parameters: \n");
		printf("drop: %f\n", args.drop);
		printf("-------------------------------------------------\n");

		drop = args.drop;
		Handle *h = create_filter();
		QHandle *qh = nfq_create_queue(h, 2, drop_handler, NULL);
		if (!qh) {
			fprintf(stderr, "error during nfq_create_queue()\n");
			exit(1);
		}
		if (nfq_set_mode(qh, NFQNL_COPY_NONE, 0xffff) < 0) {
			fprintf(stderr, "can't set packet_copy mode\n");
			exit(1);
		}
		start_filter(h, 4096);
		cleanup_queue(qh);
		cleanup_filter(h);
	}
	return 0;
}
