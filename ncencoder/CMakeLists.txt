cmake_minimum_required (VERSION 2.6)
project (ncencoder)
include_directories(../nclib/include)
include_directories(../filter/include)
#add_library(imp_nc STATIC IMPORTED)
#set_property(TARGET imp_nc PROPERTY IMPORTED_LOCATION ../nclib/libnc.a)
add_executable(
ncencoder 
src/ncencoder.c
src/cmd_args.c
)
set(CMAKE_C_FLAGS "-std=gnu99 -lnetfilter_queue -pthread -lrt")
target_link_libraries(ncencoder nc)

