/*
 ============================================================================
 Name        : ncencoder.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "cmd_args.h"

#include "filter.h"
#include "encoder.h"
#include "socket.h"

static int server_outgoing_handler(struct nfq_q_handle *qh,
		struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data) {
	u_int32_t id;
	struct nfqnl_msg_packet_hdr *ph;
	ph = nfq_get_msg_packet_hdr(nfa);
	if (ph) {
		id = ntohl(ph->packet_id);
	} else {
		perror("msg_packet_hdr is null");
	}
	char *buf;
	int ret = nfq_get_payload(nfa, &buf);
	if (ret < 0) {
		perror("payload is null");
		goto end;
	}
	//printf("Handle outgoing apptonc length: %d\n", ret);
	process(buf, ret);
	end: return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
}
static int server_incoming_handler(struct nfq_q_handle *qh,
		struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data) {
	u_int32_t id;
	struct nfqnl_msg_packet_hdr *ph;
	ph = nfq_get_msg_packet_hdr(nfa);
	if (ph) {
		id = ntohl(ph->packet_id);
	} else {
		perror("msg_packet_hdr is null");
	}
	char *buf;
	int ret = nfq_get_payload(nfa, &buf);
	if (ret < 0) {
		goto end;
	}
	//printf("Handle incoming ack\n");
	Ack *ack = (Ack *) buf;
//	printf("START CAN %d\n", ack->pid);
	update_params(ack);
//	cancel_encode(ack->pid);
//	printf("CANCELED\n");
	end: return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
}

int main(int argc, char **argv) {
	CommandLineArgs args;
	if (argc == 1) {
		printf("Usage: \n\n");
		cmd_show_usage();
	} else {
		cmd_parse_args(&args, argc, argv, NULL, NULL);
		if (args.eip == NULL) {
			printf("Usage: \n\n");
			cmd_show_usage();
			return 0;
		}
		printf("Encoder started with the following parameters: \n");
		printf("p: %lu\n", args.p);
		printf("n: %lu\n", args.n);
		printf("pdu: %lu\n", args.u);
		printf("i: %lu\n", args.i);
		printf("h: %lu\n", args.h);
		printf("t: %lu\n", args.t);
		printf("k: %lu\n", args.k);
		printf("m: %lu\n", args.m);
		printf("eip: %s\n", args.eip);
		printf("dip: %s\n", args.dip);
		printf("-------------------------------------------------\n");

		configure_encoder(args.p, args.u, args.i, args.h, args.t, args.n,
				args.m, args.k, args.eip, args.dip);
		prepare_encoders(send_data);

		Handle *h = create_filter();
		QHandle *qhi = create_queue(h, 0, server_incoming_handler);
		QHandle *qho = create_queue(h, 1, server_outgoing_handler);
		start_filter(h, 4096);
		cleanup_queue(qhi);
		cleanup_queue(qho);
		cleanup_filter(h);
	}

	return 0;
}
