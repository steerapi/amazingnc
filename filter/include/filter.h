/*
 * filter.h
 *
 *  Created on: Mar 7, 2012
 *      Author: steerapi
 */

#ifndef FILTER_H_
#define FILTER_H_

#include <stdio.h>
#include <stdlib.h>

#include <netdb.h>
#include <unistd.h>/* close() */
#include <string.h> /* memset() */
#include <time.h>

#include <linux/types.h>
#include <linux/netfilter.h>            /* for NF_ACCEPT */

#include <libnetfilter_queue/libnetfilter_queue.h>

#include <netinet/ip.h>
#include <inttypes.h>
#include <arpa/inet.h>

typedef struct nfq_handle Handle;
typedef struct nfq_q_handle QHandle;

Handle *create_filter() {
	Handle *h;
	printf("opening library handle\n");
	h = nfq_open();
	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}
	printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}
	return h;
}
QHandle *create_queue(Handle *h, uint16_t num, nfq_callback handler) {
	QHandle *qh = nfq_create_queue(h, num, handler, NULL);
	if (!qh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}
	if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}
	return qh;
}
void start_filter(Handle *h, int bufsize) {
	int fd;
	int rv;
	char buf[bufsize] __attribute__ ((aligned));
	fd = nfq_fd(h);
	while ((rv = recv(fd, buf, sizeof(buf), 0))) {
		if(rv > 0){
			nfq_handle_packet(h, buf, rv);
		}
//		printf("packet recv\n");
	}
}
void cleanup_queue(QHandle *qh) {
	nfq_destroy_queue(qh);
}
void cleanup_filter(Handle *h) {
	nfq_close(h);
}

#endif /* FILTER_H_ */
