/*
 * nc.h
 *
 *  Created on: Mar 7, 2012
 *      Author: Au
 */

#ifndef NC_H_
#define NC_H_

#define INFO(fmt,...) printf(fmt, ##__VA_ARGS__)
//#define DEBUG(fmt,...) printf(fmt, ##__VA_ARGS__)

#define DEBUG(fmt,...)

#include "GF256.h"
#include <inttypes.h>
#include <netinet/ip.h>
#include <string.h>
#include "nanotime.h"
#include "pipe_util.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>/* close() */
#include <arpa/inet.h>

#define TYPE_SYS 0
#define TYPE_CODE 1

#define SIZE_OF_SEED 2
#define SIZE_OF_SYS 1

typedef struct {
	struct ip iph;
	uint8_t num_block; //MAX at 255
	uint8_t pid; //MAX at 255
	uint8_t bid; //MAX at 255
	uint8_t gid; //MAX at 255
	uint16_t start;
	uint8_t type;
} NCEncapsulate;

typedef struct BufferNode {
	void *buffer;
	int length;
	struct BufferNode *next;
} BufferNode;

typedef struct EncoderInfo {
	struct ip iph;
	uint8_t bid; //MAX at 255
	uint8_t gid;
	uint16_t block_length;
	uint8_t num_block;
	void *buffer;
	int length;
	char src[50];
	char dst[50];
	BufferNode *bufnodes;
} EncoderInfo;

typedef void(*callback)(void*,int);
typedef struct Encoder {
	uint8_t pid;
	uint8_t bid;
//	int canceled;
	uint8_t extras;
	pthread_cond_t canceled_conv;
	pthread_mutex_t lock;
	pipe_producer_t *in;
	pipe_consumer_t *out;
	callback dcb;
} Encoder;

typedef struct Decoder {
//	pthread_mutex_t lock;
	void *buffer;
	uint8_t num_block;
	uint8_t num_sys;
	uint16_t *starts;
	uint16_t block_length;
	uint8_t pid;
	uint8_t bid; //MAX at 255
	uint8_t latest_gid;
	uint8_t deg;
	uint16_t length;
	pipe_producer_t *in;
	pipe_consumer_t *out;
	callback dcb;
	callback ack;
} Decoder;

typedef struct {
	struct ip iph;
	uint8_t pid;
	uint8_t bid;
} Ack;

#define MAX(a,b)         ((a < b) ?  (b) : (a))
#define MIN(a,b)         ((a > b) ?  (b) : (a))

#define NANO 1000000000
#define IPPROTO_NC 252

//#define X 1

int rand_seed(uint16_t *a, int lim);
void printBuffer(uint8_t *buffer, int length);
uint16_t csum(uint16_t * addr, int len);

#endif /* NC_H_ */
