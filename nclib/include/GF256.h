/*
 * GF256.h
 *
 *  Created on: Jan 21, 2012
 *      Author: Au
 */

#ifndef GF256_H_
#define GF256_H_

#include <stdint.h>

uint32_t size();
uint8_t power();
uint8_t fast_modulus(uint16_t x);
uint8_t gf_add(uint8_t a, uint8_t b);
uint8_t gf_mul(uint8_t a, uint8_t b);
uint8_t gf_inverse(uint8_t val);

#endif /* GF256_H_ */
