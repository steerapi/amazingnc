/*
 * socket.h
 *
 *  Created on: Mar 7, 2012
 *      Author: Au
 */

#ifndef SOCKET_H_
#define SOCKET_H_

void send_data(void *data, int length);
int socket_sendip(void *ippkt);
void prepare_socket();

#endif /* SOCKET_H_ */
