/*
 * decoder.h
 *
 *  Created on: Jan 21, 2012
 *      Author: Au
 */

#ifndef DECODER_H_
#define DECODER_H_

#include "nc.h"

int is_decodable(int num_block_sofar, int num_block, int row_length,
		uint8_t* data);
int netdecode(int dof, uint8_t *packet, int num_block, int block_length);
void prepare_decoders(callback dbc, callback ack);
void decapsulate(void *packet, int length);
void configure_decoder(uint32_t x, char *_sip, char*_cip);

#endif /* DECODER_H_ */
