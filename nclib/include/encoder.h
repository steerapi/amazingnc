/*
 * encoder.h
 *
 *  Created on: Jan 21, 2012
 *      Author: Au
 */

#ifndef ENCODER_H_
#define ENCODER_H_

#include <stdint.h>
#include <string.h>
#include "GF256.h"
#include "nc.h"

void update_params(Ack* ack);
uint16_t rand_coeffs(int num_block, uint8_t* coeffs);
void encode_block(int num_block, int block_length, uint8_t* data,
		uint8_t* coeffs, uint8_t* result);
void netencode(uint8_t *packet, int num_block, int block_length,
		uint8_t* result);
//void cancel_encode(int id);
void encapulate(void *data, int length, char *src, char *dst);
void prepare_encoders();
void process_and_advance_wo();
void process(void *packet, int length);
void configure_encoder(uint32_t _encoder_process, uint16_t _pdu, uint64_t _tl, uint64_t _sl,
		uint64_t _t, uint8_t _n, uint8_t _m, uint8_t _k, char *_encoder_ip, char *_decoder_ip);

#endif /* ENCODER_H_ */
