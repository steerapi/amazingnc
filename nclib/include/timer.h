/*
 * timer.h
 *
 *  Created on: Feb 17, 2012
 *      Author: Au
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <sys/time.h>
#include "nanotime.h"
#include <pthread.h>
#include "pipe_util.h"
#include <signal.h>
#include <assert.h>

#define MIN(X,Y) (X < Y ? X : Y)

typedef void (*action)(void*);

typedef struct TimeItem {
	pipe_producer_t *in;
	pipe_consumer_t *out;
	struct nanotime interval;
	action callback;
	action cleanup;
	void *args;
	pthread_mutex_t lock;
	pthread_cond_t conv;
	pthread_cond_t canceledv;
	int times;
	int canceled;
	uint64_t deadline;
} TimeItem;

#define NANO (uint64_t)1000000000

int null = 0;

void cleanupInterval(TimeItem *ti) {
	if (ti->in && ti->out) {
		if (ti->cleanup)
			ti->cleanup(ti->args);
		pipe_producer_free(ti->in);
		pipe_consumer_free(ti->out);
		ti->in = NULL;
		ti->out = NULL;
	}
}

void clearInterval(TimeItem *ti) {
	if (ti == NULL) {
		return;
	}
	pthread_mutex_lock(&ti->lock);
	if (ti->canceled) {
		pthread_mutex_unlock(&ti->lock);
		return;
	}
	ti->canceled = 1;
	pthread_cond_signal(&ti->conv);
	pthread_mutex_unlock(&ti->lock);
}

static void handle_interval(const void* elem_in, size_t count,
		pipe_producer_t* elem_out, void* aux) {
	TimeItem *ti = (aux);
	if (elem_in == NULL && count == 0) {
		pthread_mutex_destroy(&ti->lock);
		pthread_cond_destroy(&ti->conv);
		pthread_cond_destroy(&ti->canceledv);
		free(ti);
		return;
	}
	pthread_mutex_lock(&ti->lock);
	if (ti->canceled) {
//		if (ti->canceled == 1) {
//		}
		pthread_mutex_unlock(&ti->lock);
		cleanupInterval(ti);
		return;
	}
	pthread_mutex_unlock(&ti->lock);

	uint64_t deadline = ti->deadline;
	struct nanotime nt = nanotime_now();
	int64_t timeleft = deadline - nt.ns;
	if (timeleft < 0) {
		//callback
		if (ti->callback)
			ti->callback(ti->args);
		if (ti->times > 0) {
			ti->times = ti->times - 1;
			if (ti->times == 0) {
//				pthread_mutex_unlock(&ti->lock);
				cleanupInterval(ti);
				return;
			}
		}
		//update deadline
		ti->deadline = nanotime_now().ns + ti->interval.ns;
	}

	//queue up for next round
	timeleft = deadline - nanotime_now().ns;

	if (timeleft > 0) {
		//sleep
		struct timespec ts = { .tv_sec = timeleft / NANO, .tv_nsec = timeleft
				% NANO };
		pthread_mutex_lock(&ti->lock);
		pthread_cond_timedwait_relative_np(&ti->conv, &ti->lock, &ts);
		pthread_mutex_unlock(&ti->lock);
	}
//	pthread_mutex_unlock(&ti->lock);
	if (ti->in) {
		pipe_push(ti->in, &null, 1);
	}

	return;
}

TimeItem *setInterval(uint64_t nsec, action callback, action cleanup,
		void* args, int times) {
	TimeItem *ti;

	ti = malloc(sizeof(TimeItem));
	ti->args = NULL;
	ti->in = NULL;
	ti->out = NULL;
	pthread_mutex_init(&ti->lock, NULL);
	pthread_cond_init(&ti->conv, NULL);
	pthread_cond_init(&ti->canceledv, NULL);
	int elms_size = sizeof(TimeItem*);
	pipe_t* pipe1 = pipe_new(elms_size, 0);
	pipe_t* pipe2 = pipe_new(elms_size, 0);
	ti->in = pipe_producer_new(pipe1);
	ti->out = pipe_consumer_new(pipe2);
	pipe_connect(pipe_consumer_new(pipe1), handle_interval, ti,
			pipe_producer_new(pipe2));
	pipe_free(pipe1);
	pipe_free(pipe2);

	ti->cleanup = cleanup;
	ti->callback = callback;
	ti->args = args;
	ti->times = times;
	ti->interval.ns = nsec;
	ti->canceled = 0;
	struct nanotime nt = nanotime_now();
	ti->deadline = nt.ns + nsec;
	pipe_push(ti->in, &null, 1);
	return ti;
}

TimeItem *setTimeout(uint64_t nsec, action callback, action cleanup, void* args) {
	return setInterval(nsec, callback, cleanup, args, 1);
}

#endif /* TIMER_H_ */
