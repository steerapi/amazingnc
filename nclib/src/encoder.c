/*
 * encoder.c
 *
 *  Created on: Mar 7, 2012
 *      Author: Au
 */

#include "encoder.h"
#include "decoder.h"
#include "socket.h"
#include <assert.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

#define DEFAULT_PDU 1400
#define DEFAULT_N 16
#define DEFAULT_K 1
#define DEFAULT_M 1
#define DEFAULT_TL 1000000000 // 1 sec
#define DEFAULT_T 8000000 // 8 ms
#define DEFAULT_X 1

uint64_t pdu = DEFAULT_PDU;
uint64_t n = DEFAULT_N;
uint64_t k = DEFAULT_K;
uint64_t m = DEFAULT_M;
struct timespec tl = { .tv_sec = 1, .tv_nsec = 0 };
uint64_t t = DEFAULT_T;
uint64_t ex = DEFAULT_X;

//uint64_t last_gid = DEFAULT_N;
uint64_t sl = DEFAULT_N * DEFAULT_PDU;

uint64_t pid = 0;

EncoderInfo einfo;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

Encoder *encoder;

char ecip[50];
char esip[50];

inline uint16_t rand_coeffs(int num_block, uint8_t* coeffs) {
	int i;
	static uint16_t seed = 1;
	uint16_t startseed = seed;
	for (i = 0; i < num_block; i++) {
		coeffs[i] = (rand_seed(&seed, 256));
	}
	return startseed;
}

inline void encode_block(int num_block, int block_length, uint8_t* data,
		uint8_t* coeffs, uint8_t* result) {
	int i, j;
	uint8_t coeff;
	uint8_t* block;
	for (i = 0; i < num_block; ++i) {
		coeff = coeffs[i];
		block = data + i * block_length;
		for (j = 0; j < block_length; ++j) {
			result[j] = gf_add(result[j], gf_mul(coeff, block[j]));
		}
	}
}

inline void netencode(uint8_t *packet, int num_block, int block_length,
		uint8_t* result) {
	uint8_t coeffs[num_block];
	uint16_t seed = rand_coeffs(num_block, coeffs);
//	printf("%04x\n", *(uint16_t*)result);
//	printf("EncSeed: %04x\n", seed);
	*(uint16_t*) result = seed;
//	printf("%04x\n", *(uint16_t*)result);
//	printf("EncV: ");
//	int i;
//	for (i = 0; i < num_block; ++i) {
//		printf("%02x ", coeffs[i]);
//	}
//	printf("\n");
	encode_block(num_block, block_length, packet, coeffs,
			result + SIZE_OF_SEED);
}

static void *clearer(void *args) {
	while (1) {
		nanosleep(&tl, NULL);
		pthread_mutex_lock(&lock);
		DEBUG("clear process %d\n",einfo.length);
		if (einfo.length > 0) {
			process_and_advance_wo();
		}
		pthread_mutex_unlock(&lock);
	}
	return NULL;
}

void update_params(Ack* ack) {
	Encoder *enc = &encoder[ack->pid];
	pthread_mutex_lock(&enc->lock);
	DEBUG("enc->bid: %d, enc->bid: %d\n", enc->bid, ack->bid);
	if (enc->bid == ack->bid) {
		enc->extras = 0;
		pthread_cond_signal(&enc->canceled_conv);
	}
	pthread_mutex_unlock(&enc->lock);
}

struct ip ipencap;
void prepare_encapsulation() {
	struct ip *iph = &ipencap;
	memset(iph, 0, sizeof(struct ip));
	iph->ip_hl = 5;
	iph->ip_v = 4;
	iph->ip_tos = 0;
	iph->ip_id = htons(9999); /* the value doesn't matter here */
	iph->ip_off = 0;
	iph->ip_ttl = 255;
	iph->ip_p = IPPROTO_NC;
	iph->ip_sum = 0; /* set it to 0 before computing the actual checksum later */
}

void encapulate(void *data, int length, char *src, char *dst) {
	NCEncapsulate *packet = data;
	struct ip *iph = &packet->iph;
	memcpy(iph, &ipencap, sizeof(ipencap));
	iph->ip_len = htons(length);
	packet->start = htons(packet->start);
	inet_pton(AF_INET, src, &(iph->ip_src));
	inet_pton(AF_INET, dst, &(iph->ip_dst));
	iph->ip_sum = csum((unsigned short *) iph, sizeof(struct ip));
}

//TODO:
//*** Compress the header for the systematic start.
//*** Compress the entire datatocode
//	  Or compress the ip header only
static void encode(const void* elem_in, size_t count, pipe_producer_t* elem_out,
		void* aux) {
	Encoder *enc = aux;
	pthread_mutex_lock(&enc->lock);
	callback dcb = enc->dcb;
	EncoderInfo *e = (EncoderInfo *) elem_in;
	DEBUG("e->num_block: %d\n", e->num_block);
	if (e->num_block == 0) {
		DEBUG("e->num_block == 0\n");
		pthread_mutex_unlock(&enc->lock);
		return;
	}

	e->num_block = MIN(e->num_block,n);
	int total = e->length;
	total++; //for byte padding

	calculate: e->block_length = total / e->num_block;
	if (total % e->num_block != 0) {
		e->block_length++;
	}
	//while(e->block_length+e->num_block+sizeof(NCEncapsulate) > pdu){
	while (e->block_length > pdu) {
		e->num_block++;
		goto calculate;
	}

	enc->extras = k * m;
	DEBUG("e->num_block: %d, e->block_length: %d\n",e->num_block,e->block_length);DEBUG("enc->extras: %d\n",enc->extras);

	int length = e->num_block + e->block_length;
	int buflen = e->num_block * e->block_length;
	void *buffer = malloc(buflen);
	memset(buffer, 0, buflen);
	int sofar = 0;

	BufferNode *curr = e->bufnodes;

	//------
	uint16_t *starts = malloc(e->num_block * sizeof(uint16_t));
	memset(starts, UINT8_MAX, e->num_block*sizeof(uint16_t));
	//------
	while (curr) {
		memcpy(buffer+sofar, curr->buffer, curr->length);
		sofar += curr->length;
		//------
		int slot = sofar / e->block_length;
		if (starts[slot] == UINT16_MAX) {
			starts[slot] = sofar % e->block_length;
		}
		//------
		BufferNode *old = curr;
		curr = curr->next;
		free(old->buffer);
		free(old);
	}

	//byte padding
	DEBUG("buflen-sofar: %d\n", buflen-sofar);
	*(uint8_t*) (buffer + buflen - 1) = buflen - sofar;

	int i, j;
	int datalength = sizeof(NCEncapsulate) + length;
	NCEncapsulate *data = malloc(datalength);
	memset(data, 0, datalength);
	uint8_t *result = ((void*) data) + sizeof(NCEncapsulate);

	//data->block_length = htons(e->block_length);
	data->pid = enc->pid;
	data->bid = enc->bid;
	data->num_block = e->num_block;
	data->type = TYPE_SYS;
	for (i = 0; i < e->num_block; ++i) {
		memset(result, 0, length);
//		*(result + i) = 1;
		*result = i;
//		memcpy(result+e->num_block, buffer + i*e->block_length,
//				e->block_length);
		memcpy(result+SIZE_OF_SYS, buffer + i*e->block_length,
				e->block_length);
		data->gid = e->gid++;
		//------
		data->start = starts[i];
		//------
		int actuallength = datalength - data->num_block + SIZE_OF_SYS;
		encapulate(data, actuallength, esip, ecip);
		dcb(data, actuallength);
	}
	free(starts);

	struct timespec ts;

#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
	clock_serv_t cclock;
	mach_timespec_t mts;
	host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
	clock_get_time(cclock, &mts);
	mach_port_deallocate(mach_task_self(), cclock);
	ts.tv_sec = mts.tv_sec;
	ts.tv_nsec = mts.tv_nsec;
#else
	clock_gettime(CLOCK_REALTIME, &ts);
#endif

	DEBUG("e->gid - e->num_block: %d\n", e->gid - e->num_block);
//	if (e->gid - e->num_block >= enc->extras) {
//		goto cleanup;
//	}
	data->type = TYPE_CODE;
	for (i = 0; i < k; ++i) {

		for (j = 0; j < m; ++j) {

			memset(result, 0, length);
			netencode(buffer, e->num_block, e->block_length, result);
			data->gid = e->gid++;
			int actuallength = datalength - data->num_block + SIZE_OF_SEED;
			encapulate(data, actuallength, esip, ecip);
			dcb(data, actuallength);
		}

		if (e->gid - e->num_block >= enc->extras) {
			goto cleanup;
		}

		uint64_t nsec = ts.tv_nsec + t;
		ts.tv_sec += nsec / NANO;
		ts.tv_nsec = nsec % NANO;

		pthread_cond_timedwait(&enc->canceled_conv, &enc->lock, &ts);

		if (e->gid - e->num_block >= enc->extras) {
			goto cleanup;
		}
	}

	cleanup: enc->bid++;
	free(data);
	free(buffer);
	pthread_mutex_unlock(&enc->lock);
}

void prepare_encoders(callback dcb) {
	int i;
	int elms_size = sizeof(EncoderInfo);
	encoder = malloc(ex * sizeof(Encoder));
	for (i = 0; i < ex; ++i) {
		Encoder *enc = &encoder[i];
		pthread_mutex_init(&enc->lock, NULL);
		pthread_cond_init(&enc->canceled_conv, NULL);
		pipe_t* pipe1 = pipe_new(elms_size, 0);
		pipe_t* pipe2 = pipe_new(elms_size, 0);
		enc->pid = i;
		enc->bid = 0;
		enc->in = pipe_producer_new(pipe1);
		enc->out = pipe_consumer_new(pipe2);
		enc->dcb = dcb;
		pipe_connect(pipe_consumer_new(pipe1), encode, enc,
				pipe_producer_new(pipe2));
		pipe_free(pipe1);
		pipe_free(pipe2);
	}
	//pthread_mutex_init(&lock, NULL);
	prepare_encapsulation();
	prepare_socket();
	pthread_t th;
	pthread_create(&th, NULL, clearer, NULL);
}
void process_and_advance_wo() {
	if (einfo.bufnodes != NULL) {
		Encoder *enc = &encoder[pid];
		pipe_push(enc->in, &einfo, 1);
		pid = (pid + 1) % ex;
		einfo.bufnodes = NULL;
		memset(&einfo, 0, sizeof(EncoderInfo));
	}
}

BufferNode *curr;
void process(void *packet, int length) {
	pthread_mutex_lock(&lock);
	//Append the buffer
	BufferNode *node = malloc(sizeof(BufferNode));
	memset(node, 0, sizeof(BufferNode));
	node->next = NULL;
	node->buffer = malloc(length);
	memset(node->buffer, 0, length);
	node->length = length;

	memcpy(node->buffer, packet, length);
	printBuffer(node->buffer, length);
	if (einfo.bufnodes == NULL) {
		//reset encoder info
		einfo.bufnodes = node;
		curr = einfo.bufnodes;
	} else {
		curr->next = node;
		curr = node;
	}
	einfo.length += length;
	einfo.num_block++;
	//Check time limit and size limit
	if (einfo.length >= sl) {
		process_and_advance_wo();
	}
	pthread_mutex_unlock(&lock);
}
//int dynamic = 0;
//int dmin = 0;
//int dmax = 60;
void configure_encoder(uint32_t _encoder_process, uint16_t _pdu, uint64_t _tl,
		uint64_t _sl, uint64_t _t, uint8_t _n, uint8_t _m, uint8_t _k,
		char *_encoder_ip, char *_decoder_ip) {
	ex = _encoder_process;
	struct nanotime nt = nanotime_from_nsec(_tl);
	tl = nanotime_timespec(&nt);
	t = _t;
	n = _n;
	k = _k;
	m = _m;
	pdu = _pdu;
//	dynamic = _dyn;
//	dmin = _dmin;
//	dmax = _dmax;
	strcpy(esip, _encoder_ip);
	strcpy(ecip, _decoder_ip);

	//last_gid = n - 1;
	sl = _sl;
}

