/*
 * decoder.c
 *
 *  Created on: Mar 7, 2012
 *      Author: Au
 */

#include "decoder.h"
#include "socket.h"
#include <assert.h>

Decoder *decoder;
int dx = 1;
char dcip[50];
char dsip[50];

#define M(i,r) data[i*row_length+r]

void swap(int num_block_sofar, int num_block, int row_length, uint8_t* data) {
	int rank = 0;
	int length = row_length;
	int num = num_block_sofar;
	int r, i, j, idx, trank;
	uint8_t t;

	for (r = 0; r < num_block; ++r) {
		i = rank;
		while (i < num && M(i,r) == 0) {
			i++;
		}
		if (i >= num) {
			//cannot decode
			continue;
		} else {
			rank++;
		}
		trank = rank - 1;
		if (i != trank) {
			uint8_t* tmp = malloc(length);
			memcpy(tmp, &M(i,0), length);
			memmove(&M(i,0), &M(trank,0), length);
			memcpy(&M(trank,0), tmp, length);
			free(tmp);
		}
	}
}
int is_decodable_step(int num_block_sofar, int num_block, int row_length,
		uint8_t* data) {
	int rank = 0;
	int length = row_length;
	int num = num_block_sofar;
	int r, i, j, idx, trank;
	uint8_t t;

	for (r = 0; r < num_block; ++r) {
		i = rank;
		while (i < num && M(i,r) == 0) {
			i++;
		}
		if (i >= num) {
			//cannot decode
			continue;
		} else {
			rank++;
		}
		trank = rank - 1;
		int row = trank * row_length;
		//No need inverse if it is 1
		if (data[row + r] != 1) {
			t = gf_inverse(data[row + r]);
			for (idx = 0; idx < length; ++idx) {
				data[row + idx] = gf_mul(data[row + idx], t);
			}
		}
		//Only do the new row
		j = num - 1;
		int multiplier = M(j,r);
		for (idx = 0; idx < length; ++idx) {
			uint8_t subM = gf_mul(M(trank,idx), multiplier);
			M(j,idx) = gf_add(M(j,idx), subM);
		}
	}
	if (rank == num) {
		return 1;
	} else {
		return 0;
	}
}
int is_decodable(int num_block_sofar, int num_block, int row_length,
		uint8_t* data) {
	int rank = 0;
	int length = row_length;
	int num = num_block_sofar;
	int r, i, j, idx, trank;
	uint8_t t;

	for (r = 0; r < num_block; ++r) {
		i = rank;
		while (i < num && M(i,r) == 0) {
			i++;
		}
		if (i >= num) {
			//cannot decode
			continue;
		} else {
			rank++;
		}
		trank = rank - 1;
		if (i != trank) {
			uint8_t* tmp = malloc(length);
			memcpy(tmp, &M(i,0), length);
			memmove(&M(i,0), &M(trank,0), length);
			memcpy(&M(trank,0), tmp, length);
			free(tmp);
		}
		int row = trank * row_length;
		//No need inverse if it is 1
		if (data[row + r] != 1) {
			t = gf_inverse(data[row + r]);
			for (idx = 0; idx < length; ++idx) {
				data[row + idx] = gf_mul(data[row + idx], t);
			}
		}
		//Only do the row that is not systematic
		for (j = 0; j < num; ++j) {
			int multiplier = M(j,r);
			if (multiplier == 0)
				continue;
			if (j != trank) {
				for (idx = 0; idx < length; ++idx) {
					uint8_t subM = gf_mul(M(trank,idx), multiplier);
					M(j,idx) = gf_add(M(j,idx), subM);
				}
			}
		}
	}
//	for (j = 0; j < num; ++j) {
//	printf("V: ");
//	for (i = 0; i < num_block; ++i) {
//		printf("%02x ", M(j,i));
//	}
//	printf("\n");
//	}
//	printf("----------------------\n");

	if (rank == num) {
		return 1;
	} else {
		return 0;
	}
}
int netdecode(int dof, uint8_t *packet, int num_block, int block_length) {
	int row_length = block_length + num_block;
	uint8_t isdec = is_decodable(dof, num_block, row_length, packet);
	printBuffer(packet, row_length * num_block);
	int done = isdec && dof == num_block;
	if (done) {
		//is_decodable(dof, num_block, row_length, packet);
		int j;
		DEBUG("[BEGIN] netdecode done\n");
		for (j = 0; j < num_block; ++j) {
			memmove(packet + (block_length) * j,
					packet + row_length * j + num_block, block_length);
			printBuffer(packet + (block_length) * j, 50);
		}DEBUG("[END] netdecode done\n");
	}
	return isdec;
}
void deliver(void *buffer, int length, callback dcb) {
	void *cur = buffer;
	void *end = buffer + length - 1;
//	int i = 0;
	while (cur < end) {
		struct ip *iph = cur;
		int step = ntohs(iph->ip_len);
		DEBUG("step %d\n", step);DEBUG("length %d\n", length);
		dcb(cur, step);
		cur += step;
//		i++;
	}
}

Ack ackpkg;
void prepare_ack(char *src, char *dst) {
	struct ip *iph = &ackpkg.iph;
	memset(iph, 0, sizeof(struct ip));
	iph->ip_hl = 5;
	iph->ip_v = 4;
	iph->ip_tos = 0;
	int length = sizeof(Ack);
	iph->ip_len = htons(length);
	iph->ip_id = htons(9999); /* the value doesn't matter here */
	iph->ip_off = 0;
	iph->ip_ttl = 255;
	iph->ip_p = IPPROTO_NC;
	iph->ip_sum = 0; /* set it to 0 before computing the actual checksum later */
	inet_pton(AF_INET, src, &(iph->ip_src));
	inet_pton(AF_INET, dst, &(iph->ip_dst));
	iph->ip_sum = csum((unsigned short *) iph, sizeof(struct ip));
}
Ack *get_ack_pkt(char *src, char *dst) {
	return &ackpkg;
}

int find_offset(Decoder* dec, int consec, int start_pos) {
	//Find offset of next ip packet
	int offset = UINT16_MAX;
	int l = 0;
	for (l = 0; l < consec; ++l) {
		if (dec->starts[start_pos + l] != UINT16_MAX) {
			offset = dec->block_length * l + dec->starts[start_pos + l];
			break;
		}
	}
	return offset;
}
void sys_deliver(Decoder* dec, int consec, int start_pos, int start_j,
		int offset) {
	//Deliver systematic packets;
	void *cur = dec->buffer + start_j * (dec->block_length) + offset;
	void *end = dec->buffer + (start_j + consec) * (dec->block_length) - 1;
	if (start_pos + consec == dec->num_block) {
		//depad last block
		uint8_t *pad = end;
		end = end - *pad;
//		printf("last block %d\n", *pad);
	}
	uint8_t *i;
//	printf("X: ");
//	for (i = end-16; i < end; ++i) {
//		printf("%02x ",*i);
//	}
//	printf("\n");
	//Have ip header for us to read
	while (cur < end - sizeof(struct ip)) {
		struct ip *iph = cur;
		int step = ntohs(iph->ip_len);
//		if(step==0) break;
		if (cur + step < end) { //Check if we have entire packet
			//printf("Deliver\n");
			dec->dcb(cur, step);
		}
		cur += step;
	}
}

//TODO: To handle out of order packets. Need to add timestamp and filter out the old timestamp.
//Can be relative time since the start of the app.
//4bytes (or ever expanding) datatocode id.
static void decode(const void* elem_in, size_t count, pipe_producer_t* elem_out,
		void* aux) {
	Decoder *dec = (Decoder*) aux;
	NCEncapsulate *inc = *(NCEncapsulate**) elem_in;
	uint8_t *payload = ((void*) inc) + sizeof(NCEncapsulate);

	DEBUG("inc->bid: %d, dec->bid: %d\n", inc->bid, dec->bid);DEBUG("dec->deg: %d, inc->gid: %d, dec->latest_gid: %d\n", dec->deg, inc->gid, dec->latest_gid);DEBUG("dec->num_block: %d, inc->num_block: %d\n", dec->num_block,inc->num_block);

	if (dec->deg >= inc->num_block && inc->bid == dec->bid) {
		return;
	}

	if (inc->bid != dec->bid) {
		//Try to see if we can find systematic packets
		DEBUG("dec->deg:%d dec->num_block:%d\n", dec->deg, dec->num_block);
		if (dec->deg < dec->num_block) {
			int j;
			int start_j = 0;
			int start_pos = 0;
			int consec = 0;
			//Find consecutive blocks and extract packets from the blocks
			for (j = 0; j < dec->deg; ++j) {
				void *start = dec->buffer + dec->length * j;
				void *cur = start;
				void *end = cur + dec->num_block;
				int sum = 0;
				uint8_t pos = UINT8_MAX;
				while (cur < end) {
					sum += *(uint8_t*) cur;
					if (*(uint8_t*) cur == 1) {
						pos = cur - start;
					}
					cur++;
				}
				if (sum == 1 && pos != UINT8_MAX) {
					memmove(dec->buffer + (dec->block_length) * j,
							dec->buffer + dec->length * j + dec->num_block,
							dec->block_length);
					if (start_pos + consec == pos) {
						consec++;
					} else {

						//Deliver packets
						if (consec > 0) {
							int offset = find_offset(dec, consec, start_pos);
							if (offset != UINT16_MAX) {
								sys_deliver(dec, consec, start_pos, start_j,
										offset);
							}
						}

						//New
						start_pos = pos;
						start_j = j;
						consec = 1;
					}
				}
			}
			//Deliver packets
			if (consec > 0) {
				int offset = find_offset(dec, consec, start_pos);
				if (offset != UINT16_MAX) {
					sys_deliver(dec, consec, start_pos, start_j, offset);
				}
			}
			free(dec->starts);
		}
		//New
		// Clear
		if (dec->buffer) {
			free(dec->buffer);
			dec->buffer = NULL;
		}
		//New
		dec->length = inc->iph.ip_len - sizeof(NCEncapsulate); //(inc->block_length + inc->num_block);
		DEBUG("dec->length: %d\n",dec->length);
		dec->buffer = malloc(dec->length * inc->num_block);
		dec->pid = inc->pid;
		dec->bid = inc->bid;
		dec->num_block = inc->num_block;
		dec->starts = malloc(sizeof(uint16_t) * dec->num_block);
		dec->block_length = dec->length - inc->num_block; //inc->block_length;
		dec->deg = 0;
	}memcpy(dec->buffer+dec->deg*dec->length, payload, dec->length);

	if (inc->gid < dec->num_block) {
		dec->starts[inc->gid] = inc->start;
	}

	DEBUG("dec->block_length: %d dec->num_block: %d\n",
			dec->block_length, dec->num_block);DEBUG("inc->block_length: %d inc->num_block: %d\n",
			inc->block_length, inc->num_block);
//	assert(
//			(inc->iph.ip_len-sizeof(NCEncapsulate))==dec->length);
	assert(inc->num_block==dec->num_block);
	dec->latest_gid = MAX(inc->gid,dec->latest_gid);

	DEBUG("inc->gid: %d, dec->num_block: %d\n", inc->gid, dec->num_block);
	//&& dec->deg == inc->gid
	if (inc->gid < dec->num_block - 1) {
		dec->deg++;
		free(inc);
		return;
	}
	free(inc);

	if (netdecode(dec->deg + 1, dec->buffer, dec->num_block,
			dec->block_length)) {
		dec->deg++;
		if (dec->deg >= dec->num_block) {
			//Done
			//Send ACK
			Ack *ack = &ackpkg; //get_ack_pkt(dcip, dsip);
			ack->pid = dec->pid;
			ack->bid = dec->bid; //latest_gid - (dec->num_block - 1);
			dec->ack(ack, sizeof(Ack));

			//depad
			uint8_t *buf = dec->buffer + dec->num_block * dec->block_length - 1;
			int padding = *buf;
			//assert(padding>=1);
			buf = buf - padding;
//			while (*buf == 0) {
//				buf--;
//				c++;
//			}
			DEBUG("pading: %d\n", padding);
			int dlen = (void*) buf - dec->buffer + 1;
			//Deliver
			deliver(dec->buffer, dlen, dec->dcb);
		}
	}
}

void prepare_decoders(callback dcb, callback ack) {
	int i;
	int elms_size = sizeof(void*);
	decoder = malloc(dx * sizeof(Decoder));
	for (i = 0; i < dx; ++i) {
		Decoder *dec = &decoder[i];
		pipe_t* pipe1 = pipe_new(elms_size, 0);
		pipe_t* pipe2 = pipe_new(elms_size, 0);
		dec->latest_gid = 0;
		dec->bid = -1;
		dec->in = pipe_producer_new(pipe1);
		dec->out = pipe_consumer_new(pipe2);
		dec->dcb = dcb;
		dec->ack = ack;
		pipe_connect(pipe_consumer_new(pipe1), decode, dec,
				pipe_producer_new(pipe2));
		pipe_free(pipe1);
		pipe_free(pipe2);
	}
	prepare_ack(dcip, dsip);
	prepare_socket();
}

void decapsulate(void *packet, int length) {
	NCEncapsulate *inc = packet;
	Decoder *dec = &decoder[inc->pid];

	void *buffer;
	NCEncapsulate *header;
	if(inc->type==TYPE_SYS){
		buffer = malloc(length - SIZE_OF_SYS + inc->num_block); //Negate for the seed and add num block
		memcpy(buffer, packet, sizeof(NCEncapsulate));
		memcpy(buffer+sizeof(NCEncapsulate)+inc->num_block,
			packet+sizeof(NCEncapsulate)+SIZE_OF_SYS,
			length-sizeof(NCEncapsulate)-SIZE_OF_SYS);
		uint8_t *coeffs = (uint8_t*)(buffer+sizeof(NCEncapsulate));
		memset(coeffs,0,inc->num_block);
		*(coeffs+*(uint8_t*)(packet+sizeof(NCEncapsulate))) = 1;
		header = buffer;
		header->iph.ip_len = ntohs(header->iph.ip_len) - SIZE_OF_SYS + inc->num_block;

//		printf("V: ");
//		int i;
//		for (i = sizeof(NCEncapsulate); i < sizeof(NCEncapsulate)+inc->num_block; ++i) {
//			printf("%02x ", ((uint8_t*)buffer)[i]);
//		}
//		printf("\n");
	}else if(inc->type==TYPE_CODE){
		buffer = malloc(length - SIZE_OF_SEED + inc->num_block); //Negate for the seed and add num block
		memcpy(buffer, packet, sizeof(NCEncapsulate));

		memcpy(buffer+sizeof(NCEncapsulate)+inc->num_block,
			packet+sizeof(NCEncapsulate)+SIZE_OF_SEED,
			length-sizeof(NCEncapsulate)-SIZE_OF_SEED);
		uint8_t *coeffs = (uint8_t*)(buffer+sizeof(NCEncapsulate));
		int i;
		uint16_t seed = *(uint16_t*)(packet+sizeof(NCEncapsulate));
//		printf("EncSeed %04x\n", seed);
		for (i = 0; i < inc->num_block; ++i) {
			coeffs[i]=rand_seed(&seed, 256);
		}
		header = buffer;
		header->iph.ip_len = ntohs(header->iph.ip_len) - SIZE_OF_SEED + inc->num_block;
//		printf("DecV: ");
//		for (i = 0; i < inc->num_block; ++i) {
//			printf("%02x ", coeffs[i]);
//		}
//		printf("\n");
	}
	header->start = ntohs(header->start);
	DEBUG("buffer->iph.ip_len: %d\n",buffer->iph.ip_len);
	//Send info through pipe to process. If can decode
	pipe_push(dec->in, &buffer, 1);
}

void configure_decoder(uint32_t _x, char *_sip, char *_cip) {
	dx = _x;
	strcpy(dsip, _sip);
	strcpy(dcip, _cip);
}

