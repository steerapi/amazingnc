/*
 * nc.c
 *
 *  Created on: Mar 7, 2012
 *      Author: Au
 */

#include "nc.h"

int rand_seed(uint16_t *a, int lim) {
	*a = (*a * 32719 + 3) % 32749;
	return ((*a % lim) + 1);
}

void printBuffer(uint8_t *buffer, int length) {
	DEBUG("V: ");
	int i;
	for (i = 0; i < length; ++i) {
		DEBUG("%02x ", buffer[i]);
	}
	DEBUG("\n");
}

uint16_t csum(uint16_t * addr, int len) {
	int nleft = len;
	uint32_t sum = 0;
	uint16_t *w = addr;
	uint16_t answer = 0;

	while (nleft > 1) {
		sum += *w++;
		nleft -= 2;
	}
	if (nleft == 1) {
		*(uint8_t *) (&answer) = *(uint8_t *) w;
		sum += answer;
	}
	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);
	answer = ~sum;
	return (answer);
}
