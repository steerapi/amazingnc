/*
 * socket.c
 *
 *  Created on: Mar 12, 2012
 *      Author: Au
 */

#include "socket.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

int s = 0;
struct sockaddr_in servaddr;
int one = 1;

void prepare_socket() {
//	s = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
//	setsockopt(s, IPPROTO_RAW, IP_HDRINCL, &one, sizeof(one));
//	memset(&servaddr, 0, sizeof(servaddr));
//	servaddr.sin_family = AF_INET;

}

void cleanup_socket() {
//	if (s) {
//		close(s);
//	}
}

int socket_sendip(void *ippkt) {
	s = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
		setsockopt(s, IPPROTO_RAW, IP_HDRINCL, &one, sizeof(one));
		memset(&servaddr, 0, sizeof(servaddr));
		servaddr.sin_family = AF_INET;

	struct ip *iph = ippkt;
	//char buf[50];
	//inet_ntop(AF_INET, &iph->ip_dst, buf, INET_ADDRSTRLEN);
	//printf("SEND DST %s\n", buf);
	//inet_ntop(AF_INET, &iph->ip_src, buf, INET_ADDRSTRLEN);
	//printf("SEND SRC %s\n", buf);
	int len = ntohs(iph->ip_len);
	servaddr.sin_addr.s_addr = iph->ip_dst.s_addr;
	int ret = sendto(s, ippkt, len, 0, (struct sockaddr *) &servaddr,
			sizeof(servaddr));
	close(s);
	return ret;
}

void send_data(void *data, int length) {
	socket_sendip(data);
}
