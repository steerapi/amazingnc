/*
 ============================================================================
 Name        : ultimatenc2.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "encoder.h"
#include "decoder.h"
#include <assert.h>

void testnc() {
	int num_block = 16;
	int block_length = 500;
	int length = num_block + block_length;
	int buflen = num_block * block_length;
	uint8_t *buffer = malloc(buflen);
	int i;
	int j;
	for (i = 0; i < buflen; ++i) {
		buffer[i] = rand() % 256;
	}
	uint8_t *data = malloc(buflen);
	uint8_t *result = malloc(length);
	for (i = 0; i < num_block; ++i) {
		memset(result, 0, length);
		if (rand() % 100 <= 50) {
			netencode(buffer, num_block, block_length, result);
		} else {
			*(result + i) = 1;
			memcpy(result+num_block, buffer + i*block_length, block_length);
		}memcpy(data+length*i, result, length);
		if (netdecode(i + 1, data, num_block, block_length)) {
			if (i + 1 == num_block) {
				for (j = 0; j < buflen; ++j) {
					assert(data[j] == buffer[j] && "failed nc test");
				}
			}
		}
	}
}

void test_nc() {
	int i = 0;
	while (i < 10000) {
		testnc();
		i++;
	}
}

void *generate_ip_packet(int length) {
	int size = length - sizeof(struct ip);
	struct ip ipheader;
	struct ip *iph = &ipheader;
	memset(iph, 0, sizeof(struct ip));
	iph->ip_hl = 5;
	iph->ip_v = 4;
	iph->ip_tos = 0;
	iph->ip_id = htons(9999); /* the value doesn't matter here */
	iph->ip_off = 0;
	iph->ip_ttl = 255;
	iph->ip_p = 255;
	iph->ip_sum = 0; /* set it to 0 before computing the actual checksum later */
	iph->ip_len = htons(length);
	iph->ip_sum = csum((unsigned short *) iph, sizeof(struct ip));
	void *packet = malloc(length);
	memcpy(packet, iph, sizeof(struct ip));
	int i;
	uint8_t *data = packet + sizeof(struct ip);
	for (i = 0; i < size; ++i) {
		data[i] = i;
	}
	return packet;
}
void setup() {
	configure_encoder(1, 40, (uint64_t) 10000000000, 120, (uint64_t) 80000000,
			3, 1, 1, "127.0.0.1", "127.0.0.2");
	configure_decoder(1, "127.0.0.1", "127.0.0.2");
}

//void test_decoder() {
//	int k = 10;
//	int m = 10;
//	int num_packet = 3; // min
//	int pdu = 40; // max
//	configure_encoder(1, pdu, (uint64_t) 10000000000, (uint64_t) 80000000,
//			num_packet, m, k, "127.0.0.1", "127.0.0.2");
//	configure_decoder(1, "127.0.0.1", "127.0.0.2");
//	int encode_count = 0;
//	int decode_count = 0;
//	void encoded_callback(void *data, int length) {
//		struct ip* iph = data;
//		int l = ntohs(iph->ip_len);
////		printBuffer(data, l);
//		decapsulate(data, length);
////		printf("------\n");
//		encode_count++;
//		assert(encode_count<=k*m);
//	}
//	void decoded_callback(void *data, int length) {
////		printf("DECODE\n");
//		struct ip* iph = data;
//		int l = ntohs(iph->ip_len);
////		printBuffer(data, l);
////		printf("------\n");
//		decode_count++;
//		assert(decode_count<=num_packet);
//	}
//	void ack_callback(void *data, int length) {
//		update_params(data);
////		printf("ACK\n");
//		struct ip* iph = data;
//		int l = ntohs(iph->ip_len);
////		printBuffer(data, l);
//		Ack *ack = data;
////		printf("ack->extras: %d\n", ack->extras);
////		printf("ack->pid: %d\n", ack->pid);
////		printf("------\n");
//	}
//	prepare_encoders(encoded_callback);
//	prepare_decoders(decoded_callback, ack_callback);
//	int i;
//	for (i = 0; i < num_packet; ++i) {
//		process(generate_ip_packet(40), 40);
//	}
//}
//
//void test_ack() {
//	int k = 10;
//	int m = 10;
//	int n = 3; // min
//	int pdu = 40; // max
//	configure_encoder(1, pdu, (uint64_t) 10000000000, (uint64_t) 80000000, n, m,
//			k, "127.0.0.1", "127.0.0.2");
//	configure_decoder(1, "127.0.0.1", "127.0.0.2");
//	int encode_count = 0;
//	int decode_count = 0;
//	void *p = generate_ip_packet(pdu);
//	void encoded_callback(void *data, int length) {
//		decapsulate(data, length);
//		encode_count++;
//		assert(encode_count<k*m);
//	}
//	void decoded_callback(void *data, int length) {
//		decode_count++;
//		assert(decode_count<=n);
//	}
//	void ack_callback(void *data, int length) {
//		update_params(data);
//	}
//	prepare_encoders(encoded_callback);
//	prepare_decoders(decoded_callback, ack_callback);
//	int i;
//	for (i = 0; i < n; ++i) {
//		process(p, pdu);
//	}
//}

int tk = 1;
int tm = 1;
int tn = 3; // min
int tpdu = 40; // max
int encode_count = 0;
void *tp;

void encoded_callback_test_encoder(void *data, int length) {
	struct ip* iph = data;
//	int l = ntohs(iph->ip_len);
	uint8_t *sdu = data + sizeof(struct ip);
	printBuffer(sdu, 20);
	assert(memcmp(data, tp, 20) && "failed encoder test");
	printf("encode_count: %d\n", encode_count);
	encode_count++;
	assert(encode_count <= tn && "failed encoder test");
}

void test_encoder() {
	printf("%s\n", __PRETTY_FUNCTION__);
	tp = generate_ip_packet(tpdu);
	configure_encoder(1, tpdu, (uint64_t) 10000000000, tpdu * tn,
			(uint64_t) 80000000, tn, tm, tk, "127.0.0.1", "127.0.0.2");
	prepare_encoders(encoded_callback_test_encoder);
	int i;
	for (i = 0; i < tn; ++i) {
		process(tp, tpdu);
	}
}

int main(void) {
//	test_nc();

//	test_ack();
	test_encoder();
//	test_decoder();
//	test_loop();
//	return 0;
//	configure_decoder(1, "127.0.0.1", "127.0.0.2");
//	prepare_decoders();
//	pthread_t eth;
//	pthread_create(&eth, NULL, encode_thread, NULL);
//	pthread_t dth;
//	pthread_create(&dth, NULL, decode_thread, NULL);

//	int i = 0;
//	struct timespec ts = { .tv_sec = 1, .tv_nsec = 0 };
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);

//	sleep(1);
//	printf("\n\n\n\n\n");
//
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);
//	process(packet, 20);

//
//	process(packet, 12);
//	process(packet, 12);
//
//	process(packet, 12);
//	process(packet, 12);
//
//	process(packet, 12);
//	process(packet, 12);

//	while (i < 500) {
//		process(packet, 11);
//		process(packet, 12);
//////	sleep(1);
////	process(packet, 20);
////	sleep(2);
//		process(packet, 5);
//		i++;
////		process(packet, 20);
////		process(packet, 20);
////		sleep(5);
////		nanosleep(&ts, NULL);
////	process(packet, 20);
//	}
//	process(packet, 20);
//	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */
//	puts("sleeping\n");
	sleep(1000);
	return EXIT_SUCCESS;
}
