/*
 ============================================================================
 Name        : ultimatenc2.c
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "encoder.h"
#include "decoder.h"
#include <assert.h>

void *generate_ip_packet(int length) {
	int size = length - sizeof(struct ip);
	struct ip ipheader;
	struct ip *iph = &ipheader;
	memset(iph, 0, sizeof(struct ip));
	iph->ip_hl = 5;
	iph->ip_v = 4;
	iph->ip_tos = 0;
	iph->ip_id = htons(9999); /* the value doesn't matter here */
	iph->ip_off = 0;
	iph->ip_ttl = 255;
	iph->ip_p = 255;
	iph->ip_sum = 0; /* set it to 0 before computing the actual checksum later */
	iph->ip_len = htons(length);
	iph->ip_sum = csum((unsigned short *) iph, sizeof(struct ip));
	void *packet = malloc(length);
	memcpy(packet, iph, sizeof(struct ip));
	int i;
	uint8_t *data = packet + sizeof(struct ip);
	for (i = 0; i < size; ++i) {
		data[i] = i;
	}
	return packet;
}

int tk = 1;
int tm = 1;
int tn = 3; // min
int tpdu = 200; // max
int encode_count = 0;
void *tp;

void encoded_callback(void *data, int length) {
	struct ip* iph = data;
//	int l = ntohs(iph->ip_len);
	uint8_t *sdu = data + sizeof(struct ip);
	printBuffer(sdu, 20);
}

void test_encoder() {
	printf("%s\n", __PRETTY_FUNCTION__);
	tp = generate_ip_packet(tpdu);
	configure_encoder(1, tpdu, (uint64_t) 10000000000, tpdu*tn, (uint64_t) 80000000, tn,
			tm, tk, "127.0.0.1", "127.0.0.2");
	prepare_encoders(encoded_callback);
	int i;
	for (i = 0; i < tn; ++i) {
		process(tp, tpdu);
	}
}

int main(void) {
	test_encoder();
	sleep(1000);
	return EXIT_SUCCESS;
}
