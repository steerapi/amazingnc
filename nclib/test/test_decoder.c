/*
 ============================================================================
 Name        : ultimatenc2.c
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "encoder.h"
#include "decoder.h"
#include <assert.h>

void *generate_ip_packet(int length) {
	int size = length - sizeof(struct ip);
	struct ip ipheader;
	struct ip *iph = &ipheader;
	memset(iph, 0, sizeof(struct ip));
	iph->ip_hl = 5;
	iph->ip_v = 4;
	iph->ip_tos = 0;
	iph->ip_id = htons(9999); /* the value doesn't matter here */
	iph->ip_off = 0;
	iph->ip_ttl = 255;
	iph->ip_p = 255;
	iph->ip_sum = 0; /* set it to 0 before computing the actual checksum later */
	iph->ip_len = htons(length);
	iph->ip_sum = csum((unsigned short *) iph, sizeof(struct ip));
	void *packet = malloc(length);
	memcpy(packet, iph, sizeof(struct ip));
	int i;
	uint8_t *data = packet + sizeof(struct ip);
	for (i = 0; i < size; ++i) {
		data[i] = i;
	}
	return packet;
}

int rand2(int lim) {
	static long a = 1; // could be made the seed value
	a = (a * 32719 + 3) % 32749;
	return ((a % lim) + 1);
}

int num_round = 1000;
int num_packet = 128; // min
int tpdu = 1400; // max
int percent_drop = 0;
int decoded_count = 0;
pthread_mutex_t dclock = PTHREAD_MUTEX_INITIALIZER;
void encoded_callback(void *data, int length) {
//		DEBUG("ENCODE\n");
	struct ip* iph = data;
	int l = ntohs(iph->ip_len);
//		printBuffer(data, l);
	if (rand2(100) < percent_drop) {
		//drop
//			DEBUG("DROP ------\n");
		return;
	}
	decapsulate(data, length);
//		DEBUG("------\n");
}
void decoded_callback(void *data, int length) {
	DEBUG("DECODE\n");
	struct ip* iph = data;
	int l = ntohs(iph->ip_len);
	uint8_t *a = generate_ip_packet(l);
	uint8_t *b = data;
	int j;
	DEBUG("len: %d\n", l);
	printBuffer(data, l);
	assert(l>0);
	for (j = 0; j < l; ++j) {
		assert(a[j]==b[j]);
	}
	free(a);
	DEBUG("------\n");
	pthread_mutex_lock(&dclock);
	decoded_count++;
	pthread_mutex_unlock(&dclock);
}
void ack_callback(void *data, int length) {
	update_params(data);
//		DEBUG("ACK\n");
	struct ip* iph = data;
	int l = ntohs(iph->ip_len);
	Ack *ack = data;
//		DEBUG("ack->extras: %d\n", ack->extras);
//		DEBUG("ack->pid: %d\n", ack->pid);
//		printBuffer(data, l);
//		DEBUG("------\n");
}

int main(void) {
	//k*m + num_block cannot exceed sizeof(gid)=256
	int k = 1;
	int m = 3 + rand()%70;
	configure_encoder(1, tpdu, (uint64_t) 1000000000, tpdu*num_packet,(uint64_t) 8000000,
			num_packet, m, k, "127.0.0.1", "127.0.0.2");
	configure_decoder(1, "127.0.0.1", "127.0.0.2");
	int i, j;
	prepare_encoders(encoded_callback);
	prepare_decoders(decoded_callback, ack_callback);
	for (j = 0; j < num_round; ++j) {
		percent_drop = 10 + rand() % 40;
		for (i = 0; i < num_packet; ++i) {
			int length = tpdu + (rand() % 1000 - tpdu/3);
			DEBUG("gen: %d\n",length);
			void *packet = generate_ip_packet(length);
			process(packet, length);
			free(packet);
		}
//		sleep(1);
		pthread_mutex_lock(&dclock);
		INFO("DONE 0 with decoded_count: %d\n", decoded_count);
		pthread_mutex_unlock(&dclock);
	}
//	i = 0;
//	while (i < 50) {
//		pthread_mutex_lock(&dclock);
//		INFO("DONE 1 with decoded_count: %d\n", decoded_count);
//		pthread_mutex_unlock(&dclock);
//		i++;
//		sleep(1);
//	}
//	for (j = 0; j < num_round; ++j) {
//		percent_drop = 30;
//		for (i = 0; i < num_packet; ++i) {
//			int length = tpdu + rand() % 2000;
//			DEBUG("gen: %d\n",length);
//			void *packet = generate_ip_packet(length);
//			process(packet, length);
//			free(packet);
//		}
//		sleep(1);
//		pthread_mutex_lock(&dclock);
//		INFO("DONE 2 with decoded_count: %d\n", decoded_count);
//		pthread_mutex_unlock(&dclock);
//	}
//	i = 0;
	while (1) {
		pthread_mutex_lock(&dclock);
		INFO("DONE 3 with decoded_count: %d\n", decoded_count);
		pthread_mutex_unlock(&dclock);
		i++;
		sleep(1);
	}
	return EXIT_SUCCESS;
}

//TEST1
//# Packet in = # Packet out
//TEST2
//Ack really cancels the sending
//TEST3
//Timeout interval works correctly
