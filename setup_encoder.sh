#!/bin/sh
sudo iptables -F && sudo iptables -A OUTPUT ! -p 252 -d "$1" -j NFQUEUE --queue-num 1 && sudo iptables -A INPUT -s "$1" -d "`ifconfig eth0 | grep "inet addr" | awk -F: '{print $2}' | awk '{print $1}'`" -p 252 -j NFQUEUE --queue-num 0
sudo ./ncencoder/ncencoder --eip="`ifconfig eth0 | grep "inet addr" | awk -F: '{print $2}' | awk '{print $1}'`" --dip="$1" --p=1 --n=16 --k=2 --m=4 --tl=1000000000 --t=80000000
