/*
 ============================================================================
 Name        : ncdecoder.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "cmd_args.h"

#include "filter.h"
#include "decoder.h"
#include "socket.h"

int rand2(int lim) {
	static long a = 1; // could be made the seed value
	a = (a * 32719 + 3) % 32749;
	return ((a % lim) + 1);
}

static int client_incoming_handler(struct nfq_q_handle *qh,
		struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data) {
	u_int32_t id;
	struct nfqnl_msg_packet_hdr *ph;
	ph = nfq_get_msg_packet_hdr(nfa);
	if (ph) {
		id = ntohl(ph->packet_id);
	} else {
		perror("msg_packet_hdr is null");
	}
	char *buf;
	int ret = nfq_get_payload(nfa, &buf);
	if (ret < 0) {
		perror("payload is null");
		goto end;
	}
	//printf("Handle incoming ncencapsulate packet\n");
//	if (rand2(100) > -1) {
	decapsulate(buf, ret);
//	}else{
//		printf("discard\n");
//	}
	end: return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
}

int main(int argc, char **argv) {
	CommandLineArgs args;
	if (argc == 1) {
		printf("Usage: \n\n");
		cmd_show_usage();
	} else {
		cmd_parse_args(&args, argc, argv, NULL, NULL);
		if (args.eip == NULL) {
			printf("Usage: \n\n");
			cmd_show_usage();
			return 0;
		}
		printf("Decoder started with the following parameters: \n");
		printf("p: %lu\n", args.p);
		printf("eip: %s\n", args.eip);
		printf("dip: %s\n", args.dip);
		printf("-------------------------------------------------\n");

		configure_decoder(args.p, args.eip, args.dip);
		prepare_decoders(send_data,send_data);

		Handle *h = create_filter();
		QHandle *qh = create_queue(h, 0, client_incoming_handler);
		start_filter(h, 4096);
		cleanup_queue(qh);
		cleanup_filter(h);
	}
	return 0;
}

